use std::sync::Arc;
use std::collections::HashMap;

use redux::{Action, Store};

use super::entity::Entity;
use super::entities::EntitiesAction;


pub enum ManagerAction<T>
    where T: 'static
{
    Add(Entity, T),
    Remove(Entity),
    Update(Entity, fn(&T) -> T),
}

impl<T> Action for ManagerAction<T>
    where T: 'static
{
    type State = Manager<T>;
}


pub struct Manager<T>
    where T: 'static
{
    map: HashMap<Entity, Arc<T>>,
}

impl<T> Manager<T>
    where T: 'static
{

    #[inline(always)]
    pub fn new() -> Self {
        Manager {
            map: HashMap::new(),
        }
    }

    #[inline]
    pub fn contains(&self, entity: &Entity) -> bool {
        self.map.contains_key(entity)
    }
    #[inline]
    pub fn get(&self, entity: &Entity) -> Option<&T> {
        match self.map.get(&entity) {
            Some(component) => Some(&**component),
            None => None,
        }
    }

    #[inline]
    fn add(&self, entity: Entity, component: T) -> Self {
        let mut map = self.map.clone();

        map.insert(entity, Arc::new(component));

        Manager {
            map: map,
        }
    }
    #[inline]
    fn remove(&self, entity: Entity) -> Self {
        let mut map = self.map.clone();

        map.remove(&entity);

        Manager {
            map: map,
        }
    }
    #[inline]
    fn update(&self, entity: Entity, update: fn(&T) -> T) -> Self {
        let component = update(&**self.map.get(&entity).unwrap());
        let mut map = self.map.clone();

        map.insert(entity, Arc::new(component));

        Manager {
            map: map,
        }
    }
}


pub fn manager_reducer<T>(action: ManagerAction<T>, state: &Manager<T>) -> Manager<T>
    where T: 'static,
{
    match action {
        ManagerAction::Add(entity, component) => state.add(entity, component),
        ManagerAction::Remove(entity) => state.remove(entity),
        ManagerAction::Update(entity, update) => state.update(entity, update),
    }
}


pub fn manager_middleware<T>(action: &EntitiesAction, store: &Store)
    where T: 'static,
{
    match action {
        &EntitiesAction::Remove(ref entity) => {
            store.dispatch::<ManagerAction<T>>(ManagerAction::Remove(*entity));
        },
        _ => (),
    };
}


#[cfg(test)]
mod test {
    use redux::*;

    use super::super::entities::{Entities, EntitiesAction, entities_reducer};
    use super::super::entity::Entity;
    use super::*;


    #[derive(Debug, PartialEq)]
    struct Position([f32; 2]);

    impl Position {

        #[inline(always)]
        fn get(&self) -> &[f32; 2] {
            &self.0
        }

        #[inline(always)]
        fn set(&self, position: [f32; 2]) -> Position {
            Position(position)
        }
    }


    #[test]
    fn test_manager() {
        let store = StoreBuilder::new()

            .reducer(Entities::new(), entities_reducer)

            .reducer(Manager::<Position>::new(), manager_reducer::<Position>)
            .middleware(manager_middleware::<Position>)

            .build();

        let entity = Entity::new();
        store.dispatch(EntitiesAction::Add(entity));

        store.dispatch(ManagerAction::Add(entity, Position([0f32, 0f32])));
        assert_eq!(
            store.get_state().get::<Manager<Position>>().unwrap().get(&entity).unwrap(),
            &Position([0f32, 0f32])
        );

        store.dispatch(ManagerAction::Update(entity, |position: &Position| {
            position.set([1f32, 1f32])
        }));
        assert_eq!(
            store.get_state().get::<Manager<Position>>().unwrap().get(&entity).unwrap().get(),
            &[1f32, 1f32]
        );
    }
}
