use std::collections::HashSet;

use redux::Action;

use super::entity::Entity;


pub enum EntitiesAction {
    Add(Entity),
    Remove(Entity),
}

impl Action for EntitiesAction {
    type State = Entities;
}


pub struct Entities {
    set: HashSet<Entity>,
}

unsafe impl Send for Entities {}
unsafe impl Sync for Entities {}

impl Entities {

    #[inline(always)]
    pub fn new() -> Self {
        Entities {
            set: HashSet::new(),
        }
    }

    #[inline(always)]
    pub fn contains(&self, entity: &Entity) -> bool {
        self.set.contains(entity)
    }

    #[inline]
    fn add(&self, entity: Entity) -> Self {
        let mut set = self.set.clone();

        set.insert(entity);

        Entities {
            set: set,
        }
    }
    #[inline]
    fn remove(&self, entity: Entity) -> Self {
        let mut set = self.set.clone();

        set.remove(&entity);

        Entities {
            set: set,
        }
    }
}


pub fn entities_reducer(action: EntitiesAction, state: &Entities) -> Entities {
    match action {
        EntitiesAction::Add(entity) => state.add(entity),
        EntitiesAction::Remove(entity) => state.remove(entity),
    }
}


#[cfg(test)]
mod test {
    use redux::*;

    use super::super::entity::Entity;
    use super::*;


    #[test]
    fn test_entities() {
        let store = StoreBuilder::new()
            .reducer(Entities::new(), entities_reducer)
            .build();

        let entity = Entity::new();

        store.dispatch(EntitiesAction::Add(entity));
        assert_eq!(store.get_state().get::<Entities>().unwrap().contains(&entity), true);

        store.dispatch(EntitiesAction::Remove(entity));
        assert_eq!(store.get_state().get::<Entities>().unwrap().contains(&entity), false);
    }
}
